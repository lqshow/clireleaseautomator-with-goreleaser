package main

import (
	"os"

	"gitlab.com/lqshow/clireleaseautomator-with-goreleaser/cmd/fooctl/command"
)

func main() {
	rootCmd := command.NewRootCommand()
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
