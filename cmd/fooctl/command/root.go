package command

import "github.com/spf13/cobra"

func NewRootCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use: "fooctl <command> [flags]",
		Run: func(cmd *cobra.Command, args []string) {
			cmd.HelpFunc()(cmd, args)
		},
	}

	cmd.AddCommand(NewCmdSay())
	cmd.AddCommand(NewCmdVersion())

	return cmd
}
